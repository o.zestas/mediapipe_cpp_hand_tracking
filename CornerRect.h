//
// Created by helios on 12/12/2022.
//

#ifndef CV_BOX_AND_BLOCK_CPP_CORNERRECT_H
#define CV_BOX_AND_BLOCK_CPP_CORNERRECT_H


#include <opencv2/core/mat.hpp>
#include "Utils.h"

class CornerRect {
private:
    int m_size_x = 100, m_size_y = 100;
    int m_pos_x, m_pos_y;
    int m_initial_pos_x;
    cv::Scalar m_color;
public:
    CornerRect(int pos_x, int pos_y);
    void update(int new_x, int new_y);
    [[nodiscard]] bool isCursorWithinRegion(int cur_pos_x, int cur_pos_y) const;
    [[nodiscard]] int getX() const;
    [[nodiscard]] int getY() const;
    [[nodiscard]] int getSizeX() const;
    [[nodiscard]] int getSizeY() const;
    [[nodiscard]] cv::Scalar getColor() const;
    [[nodiscard]] int getInitialPosX(char hand) const;
    ~CornerRect();
};


#endif //CV_BOX_AND_BLOCK_CPP_CORNERRECT_H
