cmake_minimum_required(VERSION 3.24)
project(mediapipe_cpp_hand_tracking)
set(CMAKE_CXX_STANDARD 17)
add_executable(mediapipe_cpp_hand_tracking main.cpp CornerRect.cpp CornerRect.h Utils.cpp Utils.h box_and_block.cpp box_and_block.h)

IF(UNIX)
    SET("OpenCV_DIR" "/usr/include/opencv4/opencv2/")
    find_package( OpenCV REQUIRED )

    MESSAGE(STATUS "Include dirs ${OpenCV_INCLUDE_DIRS}")
    MESSAGE(STATUS "LINK LIBRARIES ${OpenCV_LIBS}")
    target_link_libraries(mediapipe_cpp_hand_tracking ${OpenCV_LIBS})
ENDIF()