//
// Created by Orestis Zestas on 15/12/2022.
//

#ifndef CV_BOX_AND_BLOCK_CPP_BOX_AND_BLOCK_H
#define CV_BOX_AND_BLOCK_CPP_BOX_AND_BLOCK_H
#include <chrono>

#include <opencv2/opencv.hpp>
#include "CornerRect.h"
#include "Utils.h"

#ifdef MEDIAPIPE_DISABLE_GPU
#include "handlandmarksCPU.h"
#else
#include "handlandmarksGPU.h"
#endif

class box_and_block {
protected:
    const int CLOSED_LENGTH = 80;
    const int TOTAL_TIME = 63;
    const int NUM_VISIBLE_MOVABLE_BLOCKS = 4;
    const int MAX_VISIBLE_NON_MOVABLE_BLOCKS = 8;
    const int BLOCK_INITIAL_Y_POSITION = Utils::HEIGHT - 50;
    const int DIVIDER_POINTS_ARRAY[4] = {Utils::WIDTH / 2, Utils::HEIGHT / 2 + 80, Utils::WIDTH / 2,Utils::HEIGHT};
private:
    std::list<CornerRect> m_movableRectList;
    std::list<CornerRect> m_nonMovableRectList;
    void addRect(CornerRect *pRect, bool keepRemovedRect, int x_pos);
    cv::VideoCapture m_camera;
    const char m_hand;
    const std::string& m_graph_config_file;
public:
    box_and_block(int camera_id, int apiReference, const std::string &mGraphConfigFile, char hand);
    void start();
};


#endif //CV_BOX_AND_BLOCK_CPP_BOX_AND_BLOCK_H
