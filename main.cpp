#ifdef __unix__
#define CAMERA_API_REFERENCE cv::CAP_V4L2
#elif defined(_WIN32) || defined(WIN32)
#define CAMERA_API_REFERENCE cv::CAP_DSHOW
#endif

#include "box_and_block.h"

int main(int argc, char **argv)
{
    if (argc == 4) {
        std::string graph = argv[1];
        int camera_id = *argv[2] - '0';
        char hand = *argv[3];
        box_and_block bnb = box_and_block(camera_id, CAMERA_API_REFERENCE, graph, hand);
        bnb.start();
    }
    else {
        std::cerr << "ERROR: too few arguments!\nUsage: _name_ _graph_ _cameraID_(int) _hand_(char, uppercase)" << std::endl;
    }
}

