//
// Created by helios on 13/12/2022.
//

#include <opencv2/imgproc.hpp>
#include "Utils.h"


void Utils::addCornersToRects(cv::Mat &img, int const *corners_pos, int len, int t, int rt, const cv::Scalar &colorR,
                              const cv::Scalar &colorC) {

    int x = corners_pos[0];
    int y = corners_pos[1];
    int w = corners_pos[2];
    int h = corners_pos[3];
    int x1 = x+w;
    int y1 = y+h;

    if (rt != 0) {
        cv::rectangle(img, cv::Point(x,y), cv::Point(w,h),colorR, rt);
    }
    // Top Left  x,y
    cv::line(img, cv::Point(x, y), cv::Point(x + len, y), colorC, t);
    cv::line(img, cv::Point(x, y), cv::Point(x, y + len), colorC, t);
    // Top Right  x1,y
    cv::line(img, cv::Point(x1, y), cv::Point(x1 - len, y), colorC, t);
    cv::line(img, cv::Point(x1, y), cv::Point(x1, y + len), colorC, t);
    // Bottom Left  x,y1
    cv::line(img, cv::Point(x, y1), cv::Point(x + len, y1), colorC, t);
    cv::line(img, cv::Point(x, y1), cv::Point(x, y1 - len), colorC, t);
    // Bottom Right  x1,y1
    cv::line(img, cv::Point(x1, y1), cv::Point(x1 - len, y1), colorC, t);
    cv::line(img, cv::Point(x1, y1), cv::Point(x1, y1 - len), colorC, t);

}

void Utils::addTextRect(cv::Mat &img, const char *text, const int *pos, const cv::Scalar &colorR, int scale, int thickness, int offset) {
    int ox = pos[0];
    int oy = pos[1];
    cv::Size size = cv::getTextSize(text, cv::FONT_HERSHEY_PLAIN, scale, thickness, nullptr);
    int w = size.width;
    int h = size.height;

    int x1 = ox - offset;
    int y1 = oy + offset;
    int x2 = ox + w + offset;
    int y2 =  oy - h - offset;

    cv::rectangle(img, cv::Point(x1, y1), cv::Point (x2, y2), colorR, cv::FILLED);
    cv::putText(img, text, cv::Point (ox, oy), cv::FONT_HERSHEY_PLAIN, scale, cv::Scalar(255, 255, 255), thickness);
}

void Utils::makeRectsTransparent(cv::Mat &sourceImg, cv::Mat &rectsOnlyImg, float alpha) {
    cv::addWeighted(rectsOnlyImg, alpha, sourceImg, 1 - alpha, 0, sourceImg);
}


void Utils::writeXYForLandmark(const float *coordinates, int pos, int *buffer) {
    buffer[0] = static_cast<int>(coordinates[pos * 2]*WIDTH);
    buffer[1] = static_cast<int>(coordinates[pos * 2 + 1]*HEIGHT);
}

double Utils::findDistance(int x1, int x2, int y1, int y2) {
    return hypot(x2 - x1, y2 - y1);
}

cv::Scalar Utils::generateRandomColorScalar() {
    return m_possible_colors[m_distr(m_eng)];
}

void Utils::initRandomizer() {
    std::random_device rd;
    m_eng = std::mt19937(rd());
    m_distr = std::uniform_int_distribution<>(0, 3);
}

void Utils::incrementScore() {
    m_score_counter++;
}

int Utils::getScore() {
    return m_score_counter;
}

void Utils::resetScore() {
    m_score_counter = 0;
}
