//
// Created by helios on 13/12/2022.
//

#ifndef CV_BOX_AND_BLOCK_CPP_UTILS_H
#define CV_BOX_AND_BLOCK_CPP_UTILS_H

#include <random>

class Utils {
public:
    const static int WIDTH = 1280;
    const static int HEIGHT = 720;
public:
    static void addCornersToRects(cv::Mat &img, int const *corners_pos, int len, int t, int rt, cv::Scalar const &colorR, cv::Scalar const &colorC);
    static void addTextRect(cv::Mat &img, const char *text, int const *pos, cv::Scalar const &colorR, int scale, int thickness, int offset);
    static void writeXYForLandmark(float const *coordinates, int pos, int * buffer);
    static double findDistance(int x1, int x2, int y1, int y2);
    static cv::Scalar generateRandomColorScalar();
    static void initRandomizer();
    static void incrementScore();
    static int getScore();
    static void resetScore();
    static void makeRectsTransparent(cv::Mat &sourceImg, cv::Mat &rectsOnlyImg, float alpha);
private:
    inline static std::uniform_int_distribution<> m_distr;
    inline static std::mt19937 m_eng;
private:
    inline const static cv::Scalar m_possible_colors[] = {cv::Scalar(0, 0, 255), cv::Scalar(0, 255, 0),cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 255)};
    inline static int m_score_counter = 0;
};


#endif //CV_BOX_AND_BLOCK_CPP_UTILS_H
