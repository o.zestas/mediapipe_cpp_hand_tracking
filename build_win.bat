SET BAZEL_VC=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC
SET BAZEL_VS=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools
REM Use double slashes
SET PYTHON_BIN_PATH=C:\\Users\\dit16\\AppData\\Local\\Programs\\Python\\Python310\\python.exe

REM Optional vars
REM SET BAZEL_VC_FULL_VERSION="14.33.31629"
REM SET BAZEL_WINSDK_FULL_VERSION="10.0.19041.0"

bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 hand_tracking_cpu