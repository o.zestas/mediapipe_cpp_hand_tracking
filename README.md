# Hand tracking with Mediapipe C++

## Description

Mediapipe hand tracking using C++. Works on both Windows(CPU) and Linux(CPU & GPU) environments.

Currently used in research. Utilizes Computer Vision algorithms to construct virtual environments of real-life manual dexterity evaluation tests, such as the Box & Block test.

The class **HandlandmarksDetector** encapsulates all the mediapipe functionality. It is constructed with the path of the GraphConfig file, and it is ready to detect the hand landmarks in images using the GPU.

---

### Technologies

- Mediapipe
- OpenCV

---

## How to use

### Installation

- Install mediapipe and bazel in your system, [link](https://google.github.io/mediapipe/getting_started/install.html).
- Change your current directory to the mediapipe one.
- Clone this repository in the mediapipe directory:

```console
git clone https://gitlab.com/o.zestas/mediapipe_cpp_hand_tracking
```

## Build and run

Access the cloned directory, build and run the program:
```console
cd mediapipe_cpp_hand_tracking
```

## Linux
#### With CPU
(Box & Block test example.)
```console
bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 hand_tracking_cpu

GLOG_logtostderr=1 ../bazel-bin/mediapipe_cpp_hand_tracking/hand_tracking_cpu hand_tracking_desktop_live.pbtxt 0 R
```

#### With GPU
```console
bazel build -c opt --copt -DMESA_EGL_NO_X11_HEADERS --copt -DEGL_NO_X11 hand_tracking_gpu

GLOG_logtostderr=1 ../bazel-bin/mediapipe_cpp_hand_tracking/hand_tracking_gpu hand_tracking_desktop_live_gpu.pbtxt 0 R
```
## Windows (CPU ONLY)

Open a powershell window and run:
```console
.\build_win.bat
.\run_win.bat
```
