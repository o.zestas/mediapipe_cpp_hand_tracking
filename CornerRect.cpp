//
// Created by helios on 12/12/2022.
//

#include "CornerRect.h"

CornerRect::CornerRect(int pos_x, int pos_y) {
    m_pos_x = pos_x;
    m_pos_y = pos_y;
    m_initial_pos_x = pos_x;
    m_color = Utils::generateRandomColorScalar();
}

void CornerRect::update(int new_x, int new_y) {
    m_pos_x = new_x;
    m_pos_y = new_y;
}

bool CornerRect::isCursorWithinRegion(int cur_pos_x, int cur_pos_y) const {

    if (m_pos_x - 2.5*m_size_y / 2 < cur_pos_x && cur_pos_x < m_pos_x + 2.5*m_size_y / 2
    && m_pos_y - 2.5*m_size_x / 2 < cur_pos_y && cur_pos_y < m_pos_y + 2.5*m_size_x / 2) {
        return true;
    }
    return false;

}

int CornerRect::getX() const {
    return m_pos_x;
}

int CornerRect::getY() const {
    return m_pos_y;
}

int CornerRect::getSizeX() const {
    return m_size_x;
}

int CornerRect::getSizeY() const {
    return m_size_y;
}

cv::Scalar CornerRect::getColor() const {
    return m_color;
}

int CornerRect::getInitialPosX(char hand) const {
    if (hand == 'L')
        return m_initial_pos_x;
    else
        return Utils::WIDTH - m_initial_pos_x;
}


CornerRect::~CornerRect() = default;
