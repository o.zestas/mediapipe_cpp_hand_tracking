//
// Created by Orestis Zestas on 15/12/2022.
//

#include "box_and_block.h"

void box_and_block::addRect(CornerRect *pRect, bool keepRemovedRect, int x_pos) {
    if (pRect != nullptr) {
        m_movableRectList.remove_if([r=pRect](auto& x){return &x == r;});
        if (keepRemovedRect) {
            if (m_nonMovableRectList.size() < MAX_VISIBLE_NON_MOVABLE_BLOCKS)
                m_nonMovableRectList.emplace_front(*pRect);
            else {
                int tmp = MAX_VISIBLE_NON_MOVABLE_BLOCKS / 2;
                while (tmp > 0) {
                    m_nonMovableRectList.pop_back();
                    tmp--;
                }
                m_nonMovableRectList.emplace_front(*pRect);
            }
        }
        if (m_hand == 'L')
            m_movableRectList.emplace_front(CornerRect(x_pos, BLOCK_INITIAL_Y_POSITION));
        else
            m_movableRectList.emplace_front(CornerRect(Utils::WIDTH - x_pos, BLOCK_INITIAL_Y_POSITION));

    }
    else {
        if (m_hand == 'L')
            m_movableRectList.emplace_front(CornerRect(x_pos, BLOCK_INITIAL_Y_POSITION));
        else
            m_movableRectList.emplace_front(CornerRect(Utils::WIDTH - x_pos, BLOCK_INITIAL_Y_POSITION));
    }
}

box_and_block::box_and_block(int camera_id, int apiReference, const std::string &mGraphConfigFile, char hand)
        : m_graph_config_file(mGraphConfigFile),
          m_hand(hand) {
    Utils::initRandomizer();
    m_camera = cv::VideoCapture(camera_id, apiReference);
    m_camera.set(3, Utils::WIDTH);
    m_camera.set(4, Utils::HEIGHT);
    m_camera.set(cv::CAP_PROP_FPS, 60);
}

void box_and_block::start() {
    if (!m_camera.isOpened()) {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        exit(1);
    }

    auto t0 = std::chrono::high_resolution_clock::now();

    // create a window to display the images from the webcam
    cv::namedWindow("Webcam", cv::WINDOW_AUTOSIZE);

    // this will contain the image from the webcam
    cv::Mat frame(cv::Size(Utils::WIDTH,Utils::HEIGHT), CV_8UC3, cv::Mat::AUTO_STEP);
    // this will contain the final output image
    cv::Mat output_image(cv::Size(Utils::WIDTH,Utils::HEIGHT), CV_8UC3, cv::Mat::AUTO_STEP);
    // this is a placeholder only for rects. This will later be used to make them transparent
    cv::Mat rectsOnlyImg;

#ifdef MEDIAPIPE_DISABLE_GPU
    HandlandmarksDetectorCPU handlandmarksDetector(m_graph_config_file);
#else
    HandlandmarksDetectorGPU handlandmarksDetector(m_graph_config_file);
#endif

    for (int i =0; i < NUM_VISIBLE_MOVABLE_BLOCKS; i++)
        addRect(nullptr, false, 80+i*150);
    // --------
    CornerRect *r = nullptr;
    // --------
    int coordinate_xy_buffer[2];
    // --------
    int text_pos_title[] = {50, 60};
    int text_pos_hand[] = {230, 100};
    int text_pos_score[] = {50, 100};
    int text_pos_time[] = {50, 140};
    // --------
    int text_pos_over[] = {300, 300};
    int text_pos_final_score[] = {300, 400};
    // --------
    char output_text_buffer[25];
    // --------
    bool triesToMoveBlock = false;
    // --------
    while (true) {
        auto t1 = std::chrono::high_resolution_clock::now();
        auto dur = t1 - t0;
        auto secs = std::chrono::duration_cast<std::chrono::duration<float>>(dur);
        int timeRemaining = TOTAL_TIME - static_cast<int>(secs.count());

        m_camera >> frame;
        if (frame.empty()) {
            std::cerr << "ERROR! blank frame grabbed\n";
            break;
        }

        cv::flip(frame, frame, 1);
        cv::cvtColor(frame, output_image, cv::COLOR_BGR2RGBA);

        if (timeRemaining <= 0) {
            sprintf(output_text_buffer, "%s", "Test Over!");
            Utils::addTextRect(frame, output_text_buffer, text_pos_over, cv::Scalar(0, 0, 255),6, 5, 10);
            sprintf(output_text_buffer, "%s %d", "Score:", Utils::getScore());
            Utils::addTextRect(frame, output_text_buffer, text_pos_final_score, cv::Scalar(0, 0, 255),6, 5, 10);
            output_image = frame;
        }
        else {
            // Detect the landmarks in the image
            output_image = handlandmarksDetector.DetectLandmarks(output_image);
            // Draw the vertical line in the center
            cv::line(output_image, cv::Point(DIVIDER_POINTS_ARRAY[0], DIVIDER_POINTS_ARRAY[1]),
                     cv::Point(DIVIDER_POINTS_ARRAY[2],DIVIDER_POINTS_ARRAY[3]), cv::Scalar(0, 0, 255), 7);

            if (std::all_of(handlandmarksDetector.coordinates, handlandmarksDetector.coordinates + NUM_LANDMARKS * 2,
                            [](float i) { return i == 0.0; })) {
                // do nothing, no landmarks detected
                //std::cout << "\nNO LANDMARK DETECTED" << std::endl;
            } else {

                if (r == nullptr)
                    r = &m_movableRectList.front();

                Utils::writeXYForLandmark(handlandmarksDetector.coordinates, 4, coordinate_xy_buffer);
                int x1 = coordinate_xy_buffer[0];
                int y1 = coordinate_xy_buffer[1];
                Utils::writeXYForLandmark(handlandmarksDetector.coordinates, 8, coordinate_xy_buffer);
                int x2 = coordinate_xy_buffer[0];
                int y2 = coordinate_xy_buffer[1];
                double length = Utils::findDistance(x1, x2, y1, y2);

                if (length < CLOSED_LENGTH) {

                    if (!triesToMoveBlock)
                        for (auto &itr: m_movableRectList)
                            if (itr.isCursorWithinRegion(x1,y1)) {
                               r = &itr;
                               triesToMoveBlock = true;
                               break;
                            }


                    if (triesToMoveBlock) {
                        r->update(x1, y1 - 30);
                        //std::cout << "\nx: " << r->getX() << " | y: " << r->getY() << std::endl;
                        if (r->getY() >= DIVIDER_POINTS_ARRAY[1] - 40 && DIVIDER_POINTS_ARRAY[0] - 50 <= r->getX() &&
                            r->getX() <= DIVIDER_POINTS_ARRAY[0] + 50) {
                            //printf("here!!");
                            addRect(r, false, r->getInitialPosX(m_hand));
                            triesToMoveBlock = false;
                        }
                    }
                } else {
                    if ((m_hand == 'L' && r->getX() > Utils::WIDTH / 2 + r->getSizeX()/2) ||
                        (m_hand == 'R' && r->getX() < Utils::WIDTH / 2 - r->getSizeX()/2)) {
                        Utils::incrementScore();
                        r->update(r->getX(), BLOCK_INITIAL_Y_POSITION);
                        addRect(r, true, r->getInitialPosX(m_hand));
                        triesToMoveBlock = false;
                        r = nullptr;
                    } else if (triesToMoveBlock) {
                        addRect(r, false, r->getInitialPosX(m_hand));
                        triesToMoveBlock = false;
                        r = nullptr;
                    }
                }
            }

            output_image.copyTo(rectsOnlyImg);
            for (const auto &itr: m_movableRectList) {
                cv::rectangle(rectsOnlyImg, cv::Point(itr.getX() - itr.getSizeX() / 2, itr.getY() - itr.getSizeY() / 2),
                              cv::Point(itr.getX() + itr.getSizeX() / 2, itr.getY() + itr.getSizeY() / 2),
                              itr.getColor(),
                              cv::FILLED);
                int corner_pos[] = {itr.getX() - itr.getSizeX() / 2, itr.getY() - itr.getSizeY() / 2, itr.getSizeX(),
                                    itr.getSizeY()};
                Utils::addCornersToRects(rectsOnlyImg, corner_pos, 40, 5, 0, itr.getColor(), cv::Scalar(0, 0, 255));
            }

            for (const auto &itr: m_nonMovableRectList) {
                cv::rectangle(rectsOnlyImg, cv::Point(itr.getX() - itr.getSizeX() / 2, itr.getY() - itr.getSizeY() / 2),
                              cv::Point(itr.getX() + itr.getSizeX() / 2, itr.getY() + itr.getSizeY() / 2),
                              itr.getColor(),
                              cv::FILLED);
                int corner_pos[] = {itr.getX() - itr.getSizeX() / 2, itr.getY() - itr.getSizeY() / 2, itr.getSizeX(),
                                    itr.getSizeY()};
                Utils::addCornersToRects(rectsOnlyImg, corner_pos, 40, 5, 0, itr.getColor(), cv::Scalar(0, 0, 255));
            }

            Utils::makeRectsTransparent(output_image, rectsOnlyImg, 0.5);

            sprintf(output_text_buffer, "%s", "Box & Block");
            Utils::addTextRect(output_image, output_text_buffer, text_pos_title, cv::Scalar(0, 0, 255), 3, 3, 10);
            sprintf(output_text_buffer, "Hand: %c", m_hand);
            Utils::addTextRect(output_image, output_text_buffer, text_pos_hand, cv::Scalar(0, 0, 255), 2, 2, 10);
            sprintf(output_text_buffer, "%s %d", "Score", Utils::getScore());
            Utils::addTextRect(output_image, output_text_buffer, text_pos_score, cv::Scalar(0, 0, 255), 2, 2, 10);
            sprintf(output_text_buffer, "%s %ds", "Time Remaining: ", timeRemaining);
            Utils::addTextRect(output_image, output_text_buffer, text_pos_time, cv::Scalar(0, 0, 255), 2, 2, 10);
        }
        cv::imshow("Webcam", output_image);

        handlandmarksDetector.resetCoordinates();

        int key = cv::waitKey(5);
        if (key == 'q')
            break;
        else if (key == 'r') {
            Utils::resetScore();
            m_movableRectList.clear();
            m_nonMovableRectList.clear();
            for (int i =0; i < NUM_VISIBLE_MOVABLE_BLOCKS; i++)
                addRect(nullptr, false, 80+i*150);
            t0 = std::chrono::high_resolution_clock::now();
            triesToMoveBlock = false;
            r = nullptr;
        }

    }
}

